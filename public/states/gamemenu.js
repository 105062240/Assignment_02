var GameMenu = function() {};

function getranking() {
  var postsRef = firebase.database().ref('Record');
  var total_post = [];
  var name_post = [];
  var time_post = [];
  var first_count = 0;
  var second_count = 0;
  postsRef.once('value')
      .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
              var childData = childSnapshot.val();
              var name = childSnapshot.val().email;
              var bestrecord = childSnapshot.val().data;
              var year = childSnapshot.val().Year;
              var month = childSnapshot.val().Month;
              var day = childSnapshot.val().Day;
              var hours = childSnapshot.val().Hours;
              var minutes = childSnapshot.val().Minutes;
              var seconds = childSnapshot.val().Seconds;
              if (hours < 10)
                  hours = '0' + hours;
              if (minutes < 10)
                  minutes = '0' + minutes;
              if (seconds < 10)
                  seconds = '0' + seconds;
              total_post[total_post.length] = bestrecord;
              name_post[name_post.length] = name;
              time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              first_count += 1;
          });

          //add listener
          postsRef.on('child_added', function (data) {
              second_count += 1;
              if (second_count > first_count) {
                  var childData = data.val();
                  var name = data.val().email;
                  var bestrecord = data.val().data;
                  var year = data.val().Year;
                  var month = data.val().Month;
                  var day = data.val().Day;
                  var hours = data.val().Hours;
                  var minutes = data.val().Minutes;
                  var seconds = data.val().Seconds;
                  if (hours < 10)
                      hours = '0' + hours;
                  if (minutes < 10)
                      minutes = '0' + minutes;
                  if (seconds < 10)
                      seconds = '0' + seconds;
                  total_post[total_post.length] = bestrecord;
                  name_post[name_post.length] = name;
                  time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              }
          });
          for (var i = total_post.length - 1; i > 0; i--) {
              for (var j = 0; j < i; j++) {
                  if (total_post[j] < total_post[j + 1]) {
                      var tmp, tmp_name, tmp_time;

                      tmp = total_post[j];
                      total_post[j] = total_post[j + 1];
                      total_post[j + 1] = tmp;

                      tmp_name = name_post[j];
                      name_post[j] = name_post[j + 1];
                      name_post[j + 1] = tmp_name;

                      tmp_time = time_post[j];
                      time_post[j] = time_post[j + 1];
                      time_post[j + 1] = tmp_time;
                  }
              }
          }
          name1 = name_post[0];
          score1 = total_post[0];
          time1 = time_post[0];
          name2 = name_post[1];
          score2 = total_post[1];
          time2 = time_post[1];
          name3 = name_post[2];
          score3 = total_post[2];
          time3 = time_post[2];
          number_of_users = total_post.length;
      })
      .catch(e => console.log(e.message));
}
GameMenu.prototype = {

  menuConfig: {
    startY: 260,
    startX: 30
  },

  init: function () {
    this.titleText = game.make.text(game.world.centerX, 100, "NS-SHAFT", {
      font: 'bold 40pt TheMinion',
      fill: '#FDFFB5',
      align: 'center'
    });
    this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
    this.titleText.anchor.set(0.5);
    this.optionCount = 1;
  },

  create: function () {

    if (music.name !== "dangerous" && playMusic) {
      music.stop();
      music = game.add.audio('dangerous');
      music.loop = true;
      music.play();
    }
    getranking();
    game.stage.disableVisibilityChange = true;
    game.add.sprite(0, 0, 'menu-bg');
    game.add.existing(this.titleText);

    this.addMenuOption('Start', function () {
      game.state.start("Game");
    });
    this.addMenuOption('Options', function () {
      game.state.start("Options");
    });
    
  }
};

Phaser.Utils.mixinPrototype(GameMenu.prototype, mixins);
