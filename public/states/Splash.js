var Splash = function () {};
function getranking() {
  var postsRef = firebase.database().ref('Record');
  var total_post = [];
  var name_post = [];
  var time_post = [];
  var first_count = 0;
  var second_count = 0;
  postsRef.once('value')
      .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
              var childData = childSnapshot.val();
              var name = childSnapshot.val().email;
              var bestrecord = childSnapshot.val().data;
              var year = childSnapshot.val().Year;
              var month = childSnapshot.val().Month;
              var day = childSnapshot.val().Day;
              var hours = childSnapshot.val().Hours;
              var minutes = childSnapshot.val().Minutes;
              var seconds = childSnapshot.val().Seconds;
              if (hours < 10)
                  hours = '0' + hours;
              if (minutes < 10)
                  minutes = '0' + minutes;
              if (seconds < 10)
                  seconds = '0' + seconds;
              total_post[total_post.length] = bestrecord;
              name_post[name_post.length] = name;
              time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              first_count += 1;
          });

          //add listener
          postsRef.on('child_added', function (data) {
              second_count += 1;
              if (second_count > first_count) {
                  var childData = data.val();
                  var name = data.val().email;
                  var bestrecord = data.val().data;
                  var year = data.val().Year;
                  var month = data.val().Month;
                  var day = data.val().Day;
                  var hours = data.val().Hours;
                  var minutes = data.val().Minutes;
                  var seconds = data.val().Seconds;
                  if (hours < 10)
                      hours = '0' + hours;
                  if (minutes < 10)
                      minutes = '0' + minutes;
                  if (seconds < 10)
                      seconds = '0' + seconds;
                  total_post[total_post.length] = bestrecord;
                  name_post[name_post.length] = name;
                  time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              }
          });
          for (var i = total_post.length - 1; i > 0; i--) {
              for (var j = 0; j < i; j++) {
                  if (total_post[j] < total_post[j + 1]) {
                      var tmp, tmp_name, tmp_time;

                      tmp = total_post[j];
                      total_post[j] = total_post[j + 1];
                      total_post[j + 1] = tmp;

                      tmp_name = name_post[j];
                      name_post[j] = name_post[j + 1];
                      name_post[j + 1] = tmp_name;

                      tmp_time = time_post[j];
                      time_post[j] = time_post[j + 1];
                      time_post[j + 1] = tmp_time;
                  }
              }
          }
          name1 = name_post[0];
          score1 = total_post[0];
          time1 = time_post[0];
          name2 = name_post[1];
          score2 = total_post[1];
          time2 = time_post[1];
          name3 = name_post[2];
          score3 = total_post[2];
          time3 = time_post[2];
          number_of_users = total_post.length;
      })
      .catch(e => console.log(e.message));
}
Splash.prototype = {

  loadScripts: function () {
    game.load.script('style', 'lib/style.js');
    game.load.script('mixins', 'lib/mixins.js');
    game.load.script('WebFont', 'vendor/webfontloader.js');
    game.load.script('gamemenu','states/GameMenu.js');
    game.load.script('game', 'states/Game.js');
    game.load.script('gameover','states/GameOver.js');
    game.load.script('credits', 'states/Credits.js');
    game.load.script('options', 'states/Options.js');
  },

  loadBgm: function () {
    // thanks Kevin Macleod at http://incompetech.com/
    game.load.audio('dangerous', 'assets/bgm/Heart To Heart.mp3');
    game.load.audio('exit', 'assets/bgm/Exit the Premises.mp3');
  },
  // varios freebies found from google image search
  loadImages: function () {
    game.load.image('menu-bg', 'assets/images/menu-bg.jpg');
    game.load.image('options-bg', 'assets/images/options-bg.jpg');
    game.load.image('gameover-bg', 'assets/images/gameover-bg.jpg');
  },

  loadFonts: function () {
    WebFontConfig = {
      custom: {
        families: ['TheMinion'],
        urls: ['assets/style/theminion.css']
      }
    }
  },

  init: function () {
    this.loadingBar = game.make.sprite(game.world.centerX-(387/2), 400, "loading");
    this.logo       = game.make.sprite(game.world.centerX, 200, 'brand');
    this.status     = game.make.text(game.world.centerX, 380, 'Loading...', {fill: 'white'});
    utils.centerGameObjects([this.logo, this.status]);
  },

  preload: function () {
    game.add.sprite(0, 0, 'stars');
    game.add.existing(this.logo).scale.setTo(0.5);
    game.add.existing(this.loadingBar);
    game.add.existing(this.status);
    this.load.setPreloadSprite(this.loadingBar);

    this.loadScripts();
    this.loadImages();
    this.loadFonts();
    this.loadBgm();

  },

  addGameStates: function () {

    game.state.add("GameMenu",GameMenu);
    game.state.add("Game",Game);
    game.state.add("GameOver",GameOver);
    game.state.add("Credits",Credits);
    game.state.add("Options",Options);
  },

  addGameMusic: function () {
    music = game.add.audio('dangerous');
    music.loop = true;
    music.play();
  },

  create: function() {
    this.status.setText('Ready!');
    this.addGameStates();
    this.addGameMusic();
    getranking();
    setTimeout(function () {
      game.state.start("GameMenu");
    }, 1000);
  }
};
