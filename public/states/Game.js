var Game = function(game) {};
var player;
var keyboard;

var platforms = [];

var leftWall;
var leftWall2;
var rightWall;
var rightWall2;
var ceiling;
var ceiling2;
var name1;
var name2;
var name3;
var time1;
var time2;
var time3;
var score1;
var score2;
var score3;
game.global = {
   name1,
   name2,
   name3,
   time1,
   time2,
   time3,
   score1,
   score2,
   score3
}
var number_of_users;
var text1;
var text2;
var text3;
var hit = new Audio('assets/sounds/hit.wav');
var Jump = new Audio('assets/sounds/jump.ogg');
var burp = new Audio('assets/sounds/burp.mp3');
var button = new Audio('assets/sounds/button.mp3');
var st = 0;
var distance = 0;
var status = 'running';
function writeData() {
  if (number_of_users < 3 || distance > score3) {
      var Renew = new Date();
      if (distance > score1) {
          alert('You are in first place now.');
      } else if (distance > score2 && distance < score1) {
          alert('You are in second place now.')
      } else if (distance > score3 && distance < score2) {
          alert('You are in third place now.');
      }
      var user_name = prompt("Please Enter Your Name: ", "");
      var newpostref = firebase.database().ref('Record').push();
      newpostref.set({
          email: user_name,
          data: distance,
          Year: Renew.getFullYear(),
          Month: Renew.getMonth() + 1,
          Day: Renew.getDate(),
          Hours: Renew.getHours(),
          Minutes: Renew.getMinutes(),
          Seconds: Renew.getSeconds()
      });
  }
}
function getranking() {
  var postsRef = firebase.database().ref('Record');
  var total_post = [];
  var name_post = [];
  var time_post = [];
  var first_count = 0;
  var second_count = 0;
  postsRef.once('value')
      .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
              var childData = childSnapshot.val();
              var name = childSnapshot.val().email;
              var bestrecord = childSnapshot.val().data;
              var year = childSnapshot.val().Year;
              var month = childSnapshot.val().Month;
              var day = childSnapshot.val().Day;
              var hours = childSnapshot.val().Hours;
              var minutes = childSnapshot.val().Minutes;
              var seconds = childSnapshot.val().Seconds;
              if (hours < 10)
                  hours = '0' + hours;
              if (minutes < 10)
                  minutes = '0' + minutes;
              if (seconds < 10)
                  seconds = '0' + seconds;
              total_post[total_post.length] = bestrecord;
              name_post[name_post.length] = name;
              time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              first_count += 1;
          });

          //add listener
          postsRef.on('child_added', function (data) {
              second_count += 1;
              if (second_count > first_count) {
                  var childData = data.val();
                  var name = data.val().email;
                  var bestrecord = data.val().data;
                  var year = data.val().Year;
                  var month = data.val().Month;
                  var day = data.val().Day;
                  var hours = data.val().Hours;
                  var minutes = data.val().Minutes;
                  var seconds = data.val().Seconds;
                  if (hours < 10)
                      hours = '0' + hours;
                  if (minutes < 10)
                      minutes = '0' + minutes;
                  if (seconds < 10)
                      seconds = '0' + seconds;
                  total_post[total_post.length] = bestrecord;
                  name_post[name_post.length] = name;
                  time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
              }
          });
          for (var i = total_post.length - 1; i > 0; i--) {
              for (var j = 0; j < i; j++) {
                  if (total_post[j] < total_post[j + 1]) {
                      var tmp, tmp_name, tmp_time;

                      tmp = total_post[j];
                      total_post[j] = total_post[j + 1];
                      total_post[j + 1] = tmp;

                      tmp_name = name_post[j];
                      name_post[j] = name_post[j + 1];
                      name_post[j + 1] = tmp_name;

                      tmp_time = time_post[j];
                      time_post[j] = time_post[j + 1];
                      time_post[j + 1] = tmp_time;
                  }
              }
          }
          name1 = name_post[0];
          score1 = total_post[0];
          time1 = time_post[0];
          name2 = name_post[1];
          score2 = total_post[1];
          time2 = time_post[1];
          name3 = name_post[2];
          score3 = total_post[2];
          time3 = time_post[2];
          number_of_users = total_post.length;
      })
      .catch(e => console.log(e.message));
}
function createBounders () {
  leftWall = game.add.sprite(0, 0, 'wall');
  leftWall2 = game.add.sprite(0, 400, 'wall');
  game.physics.arcade.enable(leftWall);
  leftWall.body.immovable = true;
  game.physics.arcade.enable(leftWall2);
  leftWall2.body.immovable = true;
  rightWall = game.add.sprite(583, 0, 'wall');
  game.physics.arcade.enable(rightWall);
  rightWall.body.immovable = true;
  rightWall2 = game.add.sprite(583, 400, 'wall');
  game.physics.arcade.enable(rightWall2);
  rightWall2.body.immovable = true;
  ceiling = game.add.image(0, 0, 'ceiling');
  ceiling2 = game.add.image(400, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms () {
  if(game.time.now > lastTime + 600) {
      lastTime = game.time.now;
      createOnePlatform();
      distance += 1;
  }
}

function createOnePlatform () {

  var platform;
  var x = Math.random()*(600 - 96 - 40) + 20;
  var y = 600;
  var rand = Math.random() * 100;

  if(rand < 20) {
      platform = game.add.sprite(x, y, 'normal');
  } else if (rand < 40) {
      platform = game.add.sprite(x, y, 'nails');
      game.physics.arcade.enable(platform);
      platform.body.setSize(96, 15, 0, 15);
  } else if (rand < 50) {
      platform = game.add.sprite(x, y, 'conveyorLeft');
      platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
      platform.play('scroll');
  } else if (rand < 60) {
      platform = game.add.sprite(x, y, 'conveyorRight');
      platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
      platform.play('scroll');
  } else if (rand < 80) {
      platform = game.add.sprite(x, y, 'trampoline');
      platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
      platform.frame = 3;
  } else {
      platform = game.add.sprite(x, y, 'fake');
      platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
  }

  game.physics.arcade.enable(platform);
  platform.body.immovable = true;
  platforms.push(platform);

  platform.body.checkCollision.down = false;
  platform.body.checkCollision.left = false;
  platform.body.checkCollision.right = false;
}

function createPlayer() {
  player = game.add.sprite(200, 50, 'player');
  player.direction = 10;
  game.physics.arcade.enable(player);
  player.body.gravity.y = 625;
  player.animations.add('left', [0, 1, 2, 3], 8);
  player.animations.add('right', [9, 10, 11, 12], 8);
  player.animations.add('flyleft', [18, 19, 20, 21], 12);
  player.animations.add('flyright', [27, 28, 29, 30], 12);
  player.animations.add('fly', [36, 37, 38, 39], 12);
  player.life = 10;
  player.unbeatableTime = 0;
  player.touchOn = undefined;
}

function createTextsBoard () {
  var style = {fill: '#ff0000', fontSize: '20px'}
  text1 = game.add.text(10, 10, '', style);
  text2 = game.add.text(550, 10, '', style);
  text3 = game.add.text(200, 200, 'Enter 重新開始', style);
  text3.visible = false;   
}

function updatePlayer () {
  if(keyboard.left.isDown) {
      player.body.velocity.x = -300;
  } else if(keyboard.right.isDown) {
      player.body.velocity.x = 300;
  } else {
      player.body.velocity.x = 0;
  }
  setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
  var x = player.body.velocity.x;
  var y = player.body.velocity.y;

  if (x < 0 && y > 0) {
      player.animations.play('flyleft');
  }
  if (x > 0 && y > 0) {
      player.animations.play('flyright');
  }
  if (x < 0 && y == 0) {
      player.animations.play('left');
  }
  if (x > 0 && y == 0) {
      player.animations.play('right');
  }
  if (x == 0 && y != 0) {
      player.animations.play('fly');
  }
  if (x == 0 && y == 0) {
    player.frame = 8;
  }
}

function updatePlatforms () {
  for(var i=0; i<platforms.length; i++) {
      var platform = platforms[i];
      platform.body.position.y -= 2;
      if(platform.body.position.y <= -20) {
          platform.destroy();
          platforms.splice(i, 1);
      }
  }
}

function updateTextsBoard () {
  text1.setText('life:' + player.life);
  text2.setText('B' + distance);
}

function effect(player, platform) {
  if(platform.key == 'conveyorRight') {
      conveyorRightEffect(player, platform);
  }
  if(platform.key == 'conveyorLeft') {
      conveyorLeftEffect(player, platform);
  }
  if(platform.key == 'trampoline') {
      trampolineEffect(player, platform);
  }
  if(platform.key == 'nails') {
      nailsEffect(player, platform);
  }
  if(platform.key == 'normal') {
      basicEffect(player, platform);
  }
  if(platform.key == 'fake') {
      fakeEffect(player, platform);
  }
}

function conveyorRightEffect(player, platform) {
  player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
  player.body.x -= 2;
}

function trampolineEffect(player, platform) {
  platform.animations.play('jump');
  Jump.play();
  player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
  if (player.touchOn !== platform) {
      player.life -= 3;
      player.touchOn = platform;
      burp.play();
      //game.camera.flash(0xff0000, 100);
  }
}

function basicEffect(player, platform) {
  
  if (player.touchOn !== platform) {
      if(player.life < 10) {
          player.life += 1;
      }
      player.touchOn = platform;
      hit.play();
  }
  //hit.play();
}

function fakeEffect(player, platform) {
  if(player.touchOn !== platform) {
      platform.animations.play('turn');
      setTimeout(function() {
          platform.body.checkCollision.up = false;
      }, 100);
      player.touchOn = platform;
      button.play();
  }
}

function checkTouchCeiling(player) {
  if(player.body.y < 0) {
      if(player.body.velocity.y < 0) {
          player.body.velocity.y = 0;
      }
      if(game.time.now > player.unbeatableTime) {
          player.life -= 3;
          //game.camera.flash(0xff0000, 100);
          player.unbeatableTime = game.time.now + 2000;
      }
  }
}
function start () {
  if(st == 0) {
      menu();
      st = 1;
  }
}
function checkGameOver () {
  if(player.life <= 0 || player.body.y > 700) {
      gameOver();
  }
}

function gameOver () {
  writeData();
  text3.visible = true;
  platforms.forEach(function(s) {s.destroy()});
  platforms = [];
  //if(keyboard.enter.isDown) restart();
  //else if(keyboard.z.isDown) game.state.start("GameOver");
  status = 'gameOver';
}

function restart () {
  text3.visible = false;
  distance = 0;
  createPlayer();
  status = 'running';
}
Game.prototype = {

  preload: function () {
    game.load.crossOrigin = 'anonymous';
    game.load.spritesheet('player', 'assets/player.png', 32, 32);
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
  },

  addMenuOption: function(text, callback) {
    var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    var txt = game.add.text(game.world.centerX, 500, text, optionStyle);
    txt.anchor.setTo(0.5);
    txt.stroke = "rgba(0,0,0,0";
    txt.strokeThickness = 4;
    
    var onOver = function (target) {
      target.fill = "#FEFFD5";
      target.stroke = "rgba(200,200,200,0.5)";
      txt.useHandCursor = true;
    };
    var onOut = function (target) {
      target.fill = "white";
      target.stroke = "rgba(0,0,0,0)";
      txt.useHandCursor = false;
    };
    //txt.useHandCursor = true;
    txt.inputEnabled = true;
    txt.events.onInputUp.add(callback, this);
    txt.events.onInputOver.add(onOver, this);
    txt.events.onInputOut.add(onOut, this);

    this.optionCount ++;


  },

  create: function () {
      
    this.stage.disableVisibilityChange = false;
    game.add.sprite(0, 0, 'stars');
    
    
    keyboard = game.input.keyboard.addKeys({
      'enter': Phaser.Keyboard.ENTER,
      'up': Phaser.Keyboard.UP,
      'down': Phaser.Keyboard.DOWN,
      'left': Phaser.Keyboard.LEFT,
      'right': Phaser.Keyboard.RIGHT,
      'z ':Phaser.Keyboard.Z,
      'w': Phaser.Keyboard.W,
      'a': Phaser.Keyboard.A,
      's': Phaser.Keyboard.S,
      'd': Phaser.Keyboard.D
  });
  
  createBounders();
  createPlayer();
  createTextsBoard();
  
  this.addMenuOption('EXIT', function (e) {
    restart ();
    this.game.state.start("GameOver");
  });
  },
  update: function () {
    if(status == 'gameOver' && keyboard.enter.isDown) restart();
    //if(status == 'gameOver' && keyboard.Z.isDown) this.game.state.start("GameOver");
    //if(status == 'menu') menuu();
    if(status != 'running') return;
    this.physics.arcade.collide(player, platforms, effect);
    this.physics.arcade.collide(player, [leftWall, rightWall]);
    this.physics.arcade.collide(player, [leftWall2, rightWall2]);
    checkTouchCeiling(player);
    checkGameOver();
    //start();
    updatePlayer();
    updatePlatforms();
    updateTextsBoard();

    createPlatforms();
  }
};