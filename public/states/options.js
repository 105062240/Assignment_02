var Options = function(game) {};
var name1_text;
var name2_text;
var name3_text;
var time1_text;
var time2_text;
var time3_text;
var score1_text;
var score2_text;
var score3_text;
Options.prototype = {

  menuConfig: {
    className: "inverse",
    startY: 260,
    startX: "center"
  },
  addMenuOption: function(text, callback) {
    var optionStyle = { font: '40pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    var txt = game.add.text(game.world.centerX, 500, text, optionStyle);
    txt.anchor.setTo(0.5);
    txt.stroke = "rgba(0,0,0,0";
    txt.strokeThickness = 4;
    
    var onOver = function (target) {
      target.fill = "#FEFFD5";
      target.stroke = "rgba(200,200,200,0.5)";
      txt.useHandCursor = true;
    };
    var onOut = function (target) {
      target.fill = "white";
      target.stroke = "rgba(0,0,0,0)";
      txt.useHandCursor = false;
    };
    //txt.useHandCursor = true;
    txt.inputEnabled = true;
    txt.events.onInputUp.add(callback, this);
    txt.events.onInputOver.add(onOver, this);
    txt.events.onInputOut.add(onOut, this);

    this.optionCount ++;


  },

  preload: function () {
    //game.load.image('options-bg', 'assets/images/options-bg.jpg');
  },
  create: function () {
    game.add.sprite(0, 0, 'options-bg')
    var optionStyle = { font: '20pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    name1_text = game.add.text(0,0,'1.'+ name1 + ' ' + score1 + ' ' + time1,optionStyle);
    name1_text = game.add.text(0,40,'2.'+ name2 + ' ' + score2 + ' ' + time2,optionStyle);
    name1_text = game.add.text(0,80,'3.'+ name3 + ' ' + score3 + ' ' + time3,optionStyle);
    this.addMenuOption('BACK', function (e) {
      this.game.state.start("GameMenu");
    });
  }

};

//Phaser.Utils.mixinPrototype(Options.prototype, mixins);
